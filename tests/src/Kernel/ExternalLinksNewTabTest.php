<?php

namespace Drupal\Tests\external_links_new_tab\Kernel;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the external_links_new_tab module.
 *
 * @group external_links_new_tab
 */
class ExternalLinksNewTabTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['external_links_new_tab'];

  /**
   * Tests that external link has target/rel attributes.
   */
  public function testExternalLinksNewTab() {
    $url = Url::fromUri('https://www.example.com/');
    $this->assertTrue($url->isExternal(), 'Test url is external as expected');
    $link = new Link('External link', $url);
    $renderable_link = $link->toRenderable();
    $html = $this->container->get('renderer')->renderInIsolation($renderable_link);
    $this->assertStringContainsString('target="_blank"', $html, 'Test link has target attribute with "_blank" value as expected');
    $this->assertStringContainsString('rel="noopener"', $html, 'Test link has rel attribute with "noopener" value as expected');
  }

}
